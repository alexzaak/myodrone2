package codes.zaak.myodrone2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import codes.zaak.myodrone2.fragment.CalibrateImuFragment;
import codes.zaak.myodrone2.fragment.CalibrationTestFragment;
import codes.zaak.myodrone2.fragment.CommandsOverviewFragment;
import codes.zaak.myodrone2.fragment.RecordGestureFragment;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myodrone2.utils.UiInteractionStates;

/**
 * Created by Alexander Zaak on 18.06.16.
 */
public class CalibrationActivity extends AppCompatActivity implements UiInteractionListener.FragmentListener {


    private FragmentManager fragmentManager;

    private UiInteractionStates.Pages currentPage = UiInteractionStates.Pages.GESTURE_CALIBRATION;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();

        setFragment(currentPage);


    }

    private void setFragment(UiInteractionStates.Pages page) {

        Fragment fragment = null;

        switch (page) {
            case GESTURE_CALIBRATION:
                fragment = new RecordGestureFragment();

                break;
            case IMU_CALIBRATION:
                fragment = new CalibrateImuFragment();
                break;

            case DRONE_CONTROL_SETUP:
                fragment = new CommandsOverviewFragment();
                break;
            case CALIBRATION_TEST:
                fragment = new CalibrationTestFragment();
                break;
            default:
                fragment = new RecordGestureFragment();
                break;
        }


        // Begin the transaction
        FragmentTransaction ft = fragmentManager.beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.container, fragment);
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        ft.commit();
    }


    @Override
    public void onNextClicked(UiInteractionStates.Pages nextPage) {
        setFragment(nextPage);
    }

    @Override
    public void onBackClicked(UiInteractionStates.Pages previousPage) {
        setFragment(previousPage);
    }
}
