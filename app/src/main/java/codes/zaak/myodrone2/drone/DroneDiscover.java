package codes.zaak.myodrone2.drone;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiver;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiverDelegate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Zaak on 10.06.16.
 */
public class DroneDiscover {

    private static final String TAG = DroneDiscover.class.getName();


    private final List<DroneListener.DiscoverListener> discoverListenerList;
    private final List<ARDiscoveryDeviceService> matchingDrones;

    private final Context context;

    private ARDiscoveryService arDiscoveryService;
    private ServiceConnection serviceConnection;
    private final ARDiscoveryServicesDevicesListUpdatedReceiver devicesListUpdatedReceiver;

    private boolean isDiscoveryStarted;


    public DroneDiscover(Context context) {
        this.context = context;

        this.discoverListenerList = new ArrayList<>();
        this.matchingDrones = new ArrayList<>();
        this.devicesListUpdatedReceiver = new ARDiscoveryServicesDevicesListUpdatedReceiver(mDiscoveryListener);
    }

    /**
     * Add a listener
     * All callbacks of the interface Listener will be called within this function
     * Should be called in the main thread
     *
     * @param discoverListener an object that implements the {@link DroneListener.DiscoverListener} interface
     */
    public void addListener(DroneListener.DiscoverListener discoverListener) {
        discoverListenerList.add(discoverListener);

        notifyServiceDiscovered(matchingDrones);
    }

    /**
     * remove a listener from the listener list
     *
     * @param discoverListener an object that implements the {@link DroneListener.DiscoverListener} interface
     */
    public void removeListener(DroneListener.DiscoverListener discoverListener) {
        discoverListenerList.remove(discoverListener);
    }

    /**
     * Setup the drone discoverer
     * Should be called before starting discovering
     */
    public void setup() {
        // registerReceivers
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(context);
        localBroadcastMgr.registerReceiver(devicesListUpdatedReceiver,
                new IntentFilter(ARDiscoveryService.kARDiscoveryServiceNotificationServicesDevicesListUpdated));

        // create the service connection
        if (serviceConnection == null) {
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    arDiscoveryService = ((ARDiscoveryService.LocalBinder) service).getService();

                    if (isDiscoveryStarted) {
                        startDiscovering();
                        isDiscoveryStarted = false;
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    serviceConnection = null;
                }
            };
        }

        if (arDiscoveryService == null) {
            // if the discovery service doesn't exists, bind to it
            Intent i = new Intent(context, ARDiscoveryService.class);
            context.bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    /**
     * Cleanup the object
     * Should be called when the object is not used anymore
     */
    public void cleanup() {
        stopDiscovering();
        //close discovery service
        Log.d(TAG, "closeServices ...");

        if (arDiscoveryService != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    arDiscoveryService.stop();

                    context.unbindService(serviceConnection);
                    arDiscoveryService = null;
                }
            }).start();
        }

        // unregister receivers
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(context);
        localBroadcastMgr.unregisterReceiver(devicesListUpdatedReceiver);
    }

    /**
     * Start discovering Parrot drones
     * For Wifi drones, the device should be on the drone's network
     * When drones will be discovered, you will be notified through {@link codes.zaak.myodrone2.drone.DroneListener.DiscoverListener#onDronesListUpdated(List)}
     */
    public void startDiscovering() {
        if (arDiscoveryService != null) {
            Log.i(TAG, "Start discovering");
            mDiscoveryListener.onServicesDevicesListUpdated();
            arDiscoveryService.start();
            isDiscoveryStarted = false;
        } else {
            isDiscoveryStarted = true;
        }
    }

    /**
     * Stop discovering Parrot drones
     */
    public void stopDiscovering() {
        if (arDiscoveryService != null) {
            Log.i(TAG, "Stop discovering");
            arDiscoveryService.stop();
        }
        isDiscoveryStarted = false;
    }


    private final ARDiscoveryServicesDevicesListUpdatedReceiverDelegate mDiscoveryListener =
            new ARDiscoveryServicesDevicesListUpdatedReceiverDelegate() {
                @Override
                public void onServicesDevicesListUpdated() {
                    if (arDiscoveryService != null) {
                        // clear current list
                        matchingDrones.clear();
                        List<ARDiscoveryDeviceService> deviceList = arDiscoveryService.getDeviceServicesArray();
                        Log.i(TAG, "Result: " + deviceList);
                        if (deviceList != null) {
                            Log.i(TAG, "Result: " + deviceList.size());
                            for (ARDiscoveryDeviceService service : deviceList) {
                                matchingDrones.add(service);
                            }
                        }
                        notifyServiceDiscovered(matchingDrones);
                    }
                }
            };


    private void notifyServiceDiscovered(List<ARDiscoveryDeviceService> dronesList) {
        List<DroneListener.DiscoverListener> listenersCpy = new ArrayList<>(discoverListenerList);
        for (DroneListener.DiscoverListener listener : listenersCpy) {
            listener.onDronesListUpdated(dronesList);
        }
    }
}
