package codes.zaak.myodrone2.drone;

import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;

import java.util.List;

/**
 * Created by Alexander Zaak on 10.06.16.
 */
public class DroneListener {

    public interface DiscoverListener {
        /**
         * Called when the list of seen drones is updated
         * Called in the main thread
         *
         * @param dronesList list of ARDiscoveryDeviceService which represents all available drones
         *                   Content of this list respect the drone types given in startDiscovery
         */
        void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList);
    }
    public interface DroneInteractionListener {

        /**
         * Called when the connection to the drone changes
         * Called in the main thread
         * @param state the state of the drone
         */
        void onDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state);


        /**
         * Called when the battery charge changes
         * Called in the main thread
         * @param batteryPercentage the battery remaining (in percent)
         */
        void onBatteryChargeChanged(int batteryPercentage);

        /**
         * Called when the piloting state changes
         * Called in the main thread
         * @param state the piloting state of the drone
         */
        void onPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state);


    }
}
