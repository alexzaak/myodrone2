package codes.zaak.myodrone2.controller;

import android.app.Application;
import android.content.Context;
import android.os.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import ch.qos.logback.classic.android.BasicLogcatConfigurator;
import codes.zaak.myodrone2.utils.Const;
import codes.zaak.myodrone2.utils.LoggerConfig;
import codes.zaak.myorecognizer.MyoHub;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Alexander Zaak on 07.04.15.
 */
public class App extends Application {

    static {
        BasicLogcatConfigurator.configureDefaultContext();
    }

    Logger logger = LoggerFactory.getLogger(App.class);
    /**
     * The drone is kept in the application context so that all activities use the same drone instance
     */

    private static App mInstance;
    private static Context context;
    private static MyoHub myoHub;

    @Override
    public void onCreate() {

        mInstance = this;
        new LoggerConfig().configure();

        //create
        File log = Environment.getExternalStoragePublicDirectory(Const.LOG_DIRECTORY);
        createWorkingDir(log);

        context = getApplicationContext();

//Database initializations
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        super.onCreate();

    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return context;
    }

    public static synchronized MyoHub getMyoHub() {
        return myoHub;
    }

    public static void setMyoHub(MyoHub mMyoHub) {
        myoHub = mMyoHub;
    }

    private void createWorkingDir(File dir) {
        if (!dir.exists())
            dir.mkdirs();
    }


}
