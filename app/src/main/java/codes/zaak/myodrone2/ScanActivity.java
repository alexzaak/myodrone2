package codes.zaak.myodrone2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.layout.MyoAdapter;
import codes.zaak.myodrone2.layout.RecyclerTouchListener;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myorecognizer.MyoController;
import codes.zaak.myorecognizer.MyoDiscovery;
import codes.zaak.myorecognizer.MyoGattCallback;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.commands.MyoCommands;

/**
 * Created by Alexander Zaak on 18.06.16.
 */
public class ScanActivity extends AppCompatActivity implements MyoListener.ConnectionListener {

    private static final String TAG = ScanActivity.class.getName();
    private List<MyoController> myoControllerList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MyoAdapter mAdapter;

    private MyoDiscovery myoDiscovery;
    private ProgressBar progressBar;

    private MyoHub myoHub = null;

    private Handler mHandler;
    private int searchCounter = 0;
    private MyoListener.ScannerCallback scannerCallback = new MyoListener.ScannerCallback() {
        @Override
        public void onScanFinished(List<MyoController> myoList) {
            if (myoList.size() > 0) {
                setProgressVisibility(false);
                myoControllerList.clear();
                myoControllerList.addAll(myoList);
                searchCounter = 0;
                mAdapter.notifyDataSetChanged();
            } else if (searchCounter <= 10) {

                myoDiscovery.startDiscover(1000L, scannerCallback);

                searchCounter++;
            } else {
                setProgressVisibility(false);
                searchCounter = 0;
                makeToast(getString(R.string.no_result));

            }
        }

        @Override
        public void onScanFailed(MyoStates.ScanError scanError) {

            searchCounter = 0;
            setProgressVisibility(false);
            if (scanError == MyoStates.ScanError.BLUETOOTH_DISCONNECTED) {
                makeToast(getString(R.string.please_enable_bt));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setProgressVisibility(true);
                myoDiscovery.startDiscover(1000L, scannerCallback);
            }
        });

        myoDiscovery = new MyoDiscovery(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_scan_myo);
        mAdapter = new MyoAdapter(myoControllerList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new UiInteractionListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MyoController myoController = myoControllerList.get(position);

                if(myoHub == null) {
                    myoHub = new MyoHub(myoController, ScanActivity.this, MyoStates.ConnectionSpeed.BALANCED);
                }

                myoHub.setup(MyoCommands.EmgMode.FILTERED, MyoCommands.ImuMode.ALL, MyoCommands.ClassifierMode.DISABLED, null);
                App.setMyoHub(myoHub);
                setProgressVisibility(true);
                makeToast(String.format("%s %s", getString(R.string.connecting_to), myoController.getBluetoothDevice().getName()));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onConnectionStateChanged(MyoGattCallback myoGattCallback, MyoStates.ConnectionState connectionState) {
        Log.i(TAG, connectionState.name());
        switch (connectionState) {
            case DISCONNECTED:
                setProgressVisibility(false);
                makeToast(getString(R.string.disconnected));
                break;
            case DISCONNECTING:
                setProgressVisibility(true);
                break;
            case CONNECTING:
                setProgressVisibility(true);
                break;
            case CONNECTED:
                setProgressVisibility(false);
                makeToast(getString(R.string.connected));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void setProgressVisibility(final boolean isVisible) {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (isVisible) {
                        progressBar.setVisibility(View.VISIBLE);
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });
        }


    }

    private void makeToast(String message) {
        Toast.makeText(ScanActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
