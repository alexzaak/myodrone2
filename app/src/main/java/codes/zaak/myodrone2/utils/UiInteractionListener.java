package codes.zaak.myodrone2.utils;

import android.view.View;

import codes.zaak.myodrone2.CalibrationActivity;

/**
 * Created by Alexander Zaak on 09.06.16.
 */
public class UiInteractionListener {

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public interface FragmentListener {


        void onNextClicked(UiInteractionStates.Pages imuCalibration);

        void onBackClicked(UiInteractionStates.Pages droneControlSetup);
    }
}
