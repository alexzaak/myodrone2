package codes.zaak.myodrone2.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


/**
 * Created by Alexander Zaak on 28.05.15.
 */
public class MyoReceiver extends BroadcastReceiver {

    private OnMyoInteractionListener mListener;

    public void setListener(OnMyoInteractionListener mFragment) {
        this.mListener = mFragment;
    }

    public interface OnMyoInteractionListener {
        public void onPoseChanged(boolean isConnected, int pose);

        public void onMyoSynced(boolean isSynced);
    }

    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equalsIgnoreCase(Const.SERVICE_STATUS)) {
            Bundle extra = intent.getExtras();
            final boolean isConnected = extra.getBoolean(Const.IS_MYO_CONNECTED, false);

            int pose = extra.getInt(Const.MYO_POSE, 0);//Pose.UNKNOWN.ordinal());
            boolean isSynced = extra.getBoolean(Const.IS_MYO_SYNCED, false);

            mListener.onPoseChanged(isConnected, pose);
            mListener.onMyoSynced(isSynced);
        } else {
            mListener.onPoseChanged(false, 1);//Pose.UNKNOWN.ordinal());
            mListener.onMyoSynced(false);
        }
    }


}
