package codes.zaak.myodrone2.utils;

import android.content.Context;

import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myodrone2.model.OrientationData;
import codes.zaak.myorecognizer.Gestures;

/**
 * Created by Alexander Zaak on 16.06.16.
 */
public class DroneControlCustomizer {


    public enum DroneFlyingModes {

        HOVER_MODE("HOVER"),

        FLY_FORWARD_MODE("FORWARD"),

        FLY_BACKWARD_MODE("BACKWARD"),

        FLY_RIGHT_MODE("RIGHT"),

        FLY_LEFT_MODE("LEFT"),

        START("START"),

        STOP("STOP"),

        FLY_UP_MODE("UP"),

        FLY_DOWN_MODE("DOWN"),

        ROTATE_RIGHT_MODE("ROTATE RIGHT"),

        ROTATE_LEFT_MODE("ROTATE LEFT"),

        UNDEFINED("UNDEFINED");

        private String modeName;

        private DroneFlyingModes(String modeName) {
            this.modeName = modeName;
        }

        @Override
        public String toString() {
            return modeName;
        }

    }

    private final PrefManager prefManager;

    public static DroneFlyingModes getModes(int value) {
        switch (value) {

            case 0:
                return DroneFlyingModes.HOVER_MODE;
            case 1:
                return DroneFlyingModes.FLY_FORWARD_MODE;
            case 2:
                return DroneFlyingModes.FLY_BACKWARD_MODE;
            case 3:
                return DroneFlyingModes.FLY_RIGHT_MODE;
            case 4:
                return DroneFlyingModes.FLY_LEFT_MODE;
            case 5:
                return DroneFlyingModes.START;
            case 6:
                return DroneFlyingModes.STOP;
            case 7:
                return DroneFlyingModes.FLY_UP_MODE;
            case 8:
                return DroneFlyingModes.FLY_DOWN_MODE;
            case 9:
                return DroneFlyingModes.ROTATE_RIGHT_MODE;
            case 10:
                return DroneFlyingModes.ROTATE_LEFT_MODE;
            default:
                return DroneFlyingModes.UNDEFINED;
        }
    }


    public DroneControlCustomizer(Context context) {
        this.prefManager = new PrefManager(context, Const.CUSTOM_REMOTE_PROFILE);
    }





    public void storeModeCalibration(DroneControlMode droneControlMode) {

        this.prefManager.storeDroneMode(droneControlMode);

    }


    public DroneFlyingModes getCurrentFlyingMode(Gestures.CustomGestures currentGesture, double roll, double pitch) {
        DroneControlMode droneControlMode = this.prefManager.getDroneMode(currentGesture);
        OrientationData orientationData = droneControlMode.getOrientationData();


     /*   if (orientationData.getPitchLowerThreshold() < pitch && orientationData.getPitchUpperThreshold() > pitch) {
            if (orientationData.getRollLowerThreshold() < roll && orientationData.getRollUpperThreshold() > roll) {
                return droneControlMode.getFlyingModeName();
            }
        }*/
        return DroneFlyingModes.UNDEFINED;
    }


}
