package codes.zaak.myodrone2.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import android.provider.Settings;

import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.ScanActivity;

/**
 * Created by Alexander Zaak on 23.06.15.
 */
public class Messages {

    private static boolean isMyoNotConnectedMsgShown = false;
    private static boolean isDroneNotConnectedMsgShown = false;
    private static boolean isMyoNotSyncedMsgShown = false;

    /**
     * Method to show a "Myo Not Connected Message"
     *
     * @param ctx current context
     */
    public static void myoNotConnectedMessage(final Context ctx) {
        if (isMyoNotConnectedMsgShown) {
            return;
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(R.string.myo_not_connected)
                .setPositiveButton(R.string.myo_scan, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        isMyoNotConnectedMsgShown = false;
                        ctx.startActivity(new Intent(ctx, ScanActivity.class));
                    }
                });
        builder.create().show();
        isMyoNotConnectedMsgShown = true;
    }

    /**
     * Method to show a "Not Synced Message"
     *
     * @param ctx current context
     */
    public static void myoNotSyncedMessage(final Context ctx) {
        if (isMyoNotSyncedMsgShown) {
            return;
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(R.string.myo_not_synced)
                .setPositiveButton(R.string.help, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        isMyoNotSyncedMsgShown = false;
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(ctx.getString(R.string.myo_sync_gesture_help)));
                        ctx.startActivity(intent);
                    }
                }).setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isMyoNotSyncedMsgShown = false;
            }
        });
        builder.create().show();
        isMyoNotSyncedMsgShown = true;
    }

    /**
     * Method to show a "Drone Not Connected Message"
     *
     * @param ctx current context
     */
    public static void droneNotConnectedMessage(final Context ctx, final Vibrator vibrator) {
        if (isDroneNotConnectedMsgShown) {
            return;
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(R.string.drone_not_connected)
                .setPositiveButton(R.string.wifi_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (vibrator != null) {
                            vibrator.cancel();
                        }
                        isDroneNotConnectedMsgShown = false;
                        ctx.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (vibrator != null) {
                    vibrator.cancel();
                }
                isDroneNotConnectedMsgShown = false;
            }
        });
        builder.create().show();
        isDroneNotConnectedMsgShown = true;
    }
}
