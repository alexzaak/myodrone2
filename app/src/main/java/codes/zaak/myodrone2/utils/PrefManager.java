package codes.zaak.myodrone2.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoStates;

/**
 * Created by Alexander Zaak on 03.06.15.
 * <p>
 * Class to managing preferences
 */
public class PrefManager {

    private static final String TAG = PrefManager.class.getName();
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;


    public PrefManager(Context context) {
        this.pref = PreferenceManager.getDefaultSharedPreferences(context);
        this.editor = this.pref.edit();
    }

    public PrefManager(Context context, String prefName) {
        this.pref = context.getSharedPreferences(prefName, 0);
        this.editor = this.pref.edit();

    }


    public void storeDroneMode(DroneControlMode controlMode) {


        // this.editor.putString(controlMode.getGesture().name(), controlMode.toJson());
        // this.editor.commit();
    }

    public DroneControlMode getDroneMode(Gestures.CustomGestures gesture) {
        String flyingModeRaw = this.pref.getString(gesture.name(), null);

        if (flyingModeRaw == null) {
            return null;
        }
        return null;
    }


    public boolean isFeedBack() {
        return pref.getBoolean(Const.PREF_FEEDBACK, false);
    }

    public MyoStates.AlgorithmType getAlgorithmType() {
        String strType = pref.getString(Const.PREF_ALGORITHM, null);

        Log.i(TAG, strType);

        int type = 1;

        if (strType != null) {
            type = Integer.valueOf(strType);
        }


        return MyoStates.AlgorithmType.valueOf(type);
    }

    public boolean isRefinement() {
        return pref.getBoolean(Const.PREF_AUTOMATIC_REFINE, false);
    }
}
