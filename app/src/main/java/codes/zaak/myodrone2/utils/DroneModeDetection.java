package codes.zaak.myodrone2.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.util.List;

import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myodrone2.model.OrientationData;
import codes.zaak.myorecognizer.Gestures;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Alexander Zaak on 23.06.16.
 */
public class DroneModeDetection {
    private static final String TAG = DroneModeDetection.class.getName();
    private final Realm realm;

    private final Handler mHandler;

    public interface Listener {
        void onModeDetected(DroneControlCustomizer.DroneFlyingModes mode);
    }

    private Listener mListener;

    private DroneControlCustomizer.DroneFlyingModes detectedMode = DroneControlCustomizer.DroneFlyingModes.UNDEFINED;


    public DroneModeDetection(Handler mHandler) {
        this.mHandler = mHandler;
        this.realm = Realm.getDefaultInstance();
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public void getCurrentMode(final Gestures.CustomGestures currentGesture, final int droneState, final double roll, final double pitch) {

        if (currentGesture == Gestures.CustomGestures.UNDEFINED) {
            this.mListener.onModeDetected(DroneControlCustomizer.DroneFlyingModes.HOVER_MODE);
            return;
        }

        this.mHandler.post(new Runnable() {
            @Override
            public void run() {


                RealmResults<DroneControlMode> droneControlModeList = realm.where(DroneControlMode.class).equalTo("gestureList.gestureId", currentGesture.ordinal()).equalTo("droneState", droneState).findAll();



                if(currentGesture == Gestures.CustomGestures.GESTURE_3) {
                    Log.i(TAG, "SIZE: " + droneControlModeList.size());
                    Log.i(TAG, "ROLL: "+ roll+" PITCH: " + pitch);

                    for(DroneControlMode mode:droneControlModeList) {
                        Log.i(TAG, "Mode: "+DroneControlCustomizer.getModes(mode.getFlyingModeName()));
                        Log.i(TAG, "Orientation: "+mode.getOrientationData().toString());
                    }
                }

                if (droneControlModeList.size() > 1) {
                    droneControlModeList = droneControlModeList.where()
                            .lessThan("orientationData.maxPitchUp", pitch)
                            .greaterThan("orientationData.maxPitchDown", pitch)
                            .lessThan("orientationData.maxRollLeft", roll)
                            .greaterThan("orientationData.maxRollRight", roll).findAll();




                          /*   .or()
                           .greaterThan("orientationData.maxPitchDown", pitch)
                            .lessThan("orientationData.minPitchDown", pitch)
                            .greaterThan("orientationData.maxRollRight", roll)
                            .lessThan("orientationData.maxRollLeft", roll)
                            .or()
                            .greaterThan("orientationData.minRollLeft", roll)
                            .lessThan("orientationData.maxRollLeft", roll)
                            .greaterThan("orientationData.maxPitchDown", pitch)
                            .lessThan("orientationData.maxPitchUp", pitch)
                            .or()
                            .lessThan("orientationData.minRollRight", roll)
                            .greaterThan("orientationData.maxRollRight", roll)
                            .greaterThan("orientationData.maxPitchDown", pitch)
                            .lessThan("orientationData.maxPitchUp", pitch)
                            .findAll();*/

                    Log.i(TAG, "SELECTED SIZE: " + droneControlModeList.size());


                }

                if (!droneControlModeList.isEmpty()) {
                    int modeId = droneControlModeList.first().getFlyingModeName();

                    detectedMode = DroneControlCustomizer.getModes(modeId);

                    DroneModeDetection.this.mListener.onModeDetected(detectedMode);

                } else {
                    DroneModeDetection.this.mListener.onModeDetected(DroneControlCustomizer.DroneFlyingModes.HOVER_MODE);
                }
            }
        });


    }
}
