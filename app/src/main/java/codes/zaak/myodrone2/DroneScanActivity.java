package codes.zaak.myodrone2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;

import java.util.ArrayList;
import java.util.List;

import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.drone.DroneDiscover;
import codes.zaak.myodrone2.drone.DroneListener;
import codes.zaak.myodrone2.layout.DroneAdapter;
import codes.zaak.myodrone2.layout.RecyclerTouchListener;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myorecognizer.MyoHub;

/**
 * Created by Alexander Zaak on 18.06.16.
 */
public class DroneScanActivity extends AppCompatActivity implements DroneListener.DiscoverListener {

    public static final String EXTRA_DEVICE_SERVICE = "EXTRA_DEVICE_SERVICE";

    private static final String TAG = "DeviceListActivity";

    // this block loads the native libraries
    // it is mandatory
    static {
        ARSDK.loadSDKLibs();
    }

    private final List<ARDiscoveryDeviceService> mDronesList = new ArrayList<>();
    public DroneDiscover mDroneDiscoverer;
    private RecyclerView recyclerView;
    private DroneAdapter droneAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone_scan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final MyoHub myoHub = App.getMyoHub();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // start discovering
                    mDroneDiscoverer.startDiscovering();

                }
            });
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_drone_scan_view);

        droneAdapter = new DroneAdapter(mDronesList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(droneAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new UiInteractionListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // launch the activity related to the type of discovery device service
                Intent intent = null;

                if (myoHub == null) {
                    Toast.makeText(DroneScanActivity.this, getString(R.string.connect_to_myo), Toast.LENGTH_SHORT).show();
                    return;
                }

                ARDiscoveryDeviceService service = mDronesList.get(position);
                ARDISCOVERY_PRODUCT_ENUM product = ARDiscoveryService.getProductFromProductID(service.getProductID());
                switch (product) {
                    case ARDISCOVERY_PRODUCT_ARDRONE:
                    case ARDISCOVERY_PRODUCT_BEBOP_2:
                        intent = new Intent(DroneScanActivity.this, DroneActivity.class);
                        break;

                    case ARDISCOVERY_PRODUCT_SKYCONTROLLER:
                        //intent = new Intent(DeviceListActivity.this, SkyControllerActivity.class);
                        break;

                    case ARDISCOVERY_PRODUCT_JS:
                    case ARDISCOVERY_PRODUCT_JS_EVO_LIGHT:
                    case ARDISCOVERY_PRODUCT_JS_EVO_RACE:
                        //intent = new Intent(DeviceListActivity.this, JSActivity.class);
                        break;

                    case ARDISCOVERY_PRODUCT_MINIDRONE:
                    case ARDISCOVERY_PRODUCT_MINIDRONE_EVO_BRICK:
                    case ARDISCOVERY_PRODUCT_MINIDRONE_EVO_LIGHT:
                        //intent = new Intent(DeviceListActivity.this, MiniDroneActivity.class);
                        break;

                    default:
                        Log.e(TAG, "The type " + product + " is not supported by this sample");
                }

                if (intent != null) {
                    intent.putExtra(EXTRA_DEVICE_SERVICE, service);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        mDroneDiscoverer = new DroneDiscover(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // setup the drone discoverer and register as listener
        mDroneDiscoverer.setup();
        mDroneDiscoverer.addListener(this);

        // start discovering
        mDroneDiscoverer.startDiscovering();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // clean the drone discoverer object
        mDroneDiscoverer.stopDiscovering();
        mDroneDiscoverer.cleanup();
        mDroneDiscoverer.removeListener(this);
    }

    @Override
    public void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList) {
        mDronesList.clear();
        mDronesList.addAll(dronesList);
        droneAdapter.notifyDataSetChanged();
    }
}
