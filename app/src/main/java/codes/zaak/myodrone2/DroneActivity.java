package codes.zaak.myodrone2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;

import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.controller.DroneController;
import codes.zaak.myodrone2.controller.RealmController;
import codes.zaak.myodrone2.drone.DroneListener;
import codes.zaak.myodrone2.utils.DroneControlCustomizer;
import codes.zaak.myodrone2.utils.DroneModeDetection;
import codes.zaak.myodrone2.utils.PrefManager;
import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoController;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.commands.MyoCommands;
import codes.zaak.myorecognizer.communication.MyoCommunicator;
import codes.zaak.myorecognizer.gestures.CustomGestureController;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Orientation;
import io.realm.Realm;

public class DroneActivity extends AppCompatActivity implements DroneListener.DroneInteractionListener, MyoListener.CustomGesturesListener, MyoListener.BatteryCallback, MyoListener.ImuDataListener, DroneModeDetection.Listener {

    private static final String TAG = DroneActivity.class.getName();

    MyoHub myoHub = App.getMyoHub();

    private DroneController droneController;
    private ProgressDialog mConnectionProgressDialog;

    private TextView batteryDrone, batteryMyo, gestureCommand, speedTextView;

    private ImageView currentGesture;

    private Handler handler;

    private PrefManager prefManager;

    private Button startLandBtn, emergencyBtn;
    private SeekBar speedSeekBar;

    private boolean starting = false;

    private int droneSpeedValue = 0;

    private Realm realm;

    private DroneModeDetection droneModeDetection;

    private CustomGestureController customGestureController;

    private boolean isFeedback = false;

    private DroneControlCustomizer.DroneFlyingModes lastFlyingMode = DroneControlCustomizer.DroneFlyingModes.UNDEFINED;

    private ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM droneState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handler = new Handler();
        prefManager = new PrefManager(this);
        isFeedback = prefManager.isFeedBack();


        Intent intent = getIntent();
        ARDiscoveryDeviceService service = intent.getParcelableExtra(DroneScanActivity.EXTRA_DEVICE_SERVICE);
        droneController = new DroneController(this, service);

        droneController.addListener(this);


        speedTextView = (TextView) findViewById(R.id.drone_speed);
        startLandBtn = (Button) findViewById(R.id.start_land_btn);
        emergencyBtn = (Button) findViewById(R.id.emergency_btn);
        speedSeekBar = (SeekBar) findViewById(R.id.drone_speed_seekbar);

        currentGesture = (ImageView) findViewById(R.id.image_current_gesture);

        setText(speedTextView, String.format("%d %%", droneSpeedValue));

        speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                setText(speedTextView, String.format(getString(R.string.percent_mask), progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                droneSpeedValue = progress;
                setText(speedTextView, String.format("%d %%", droneSpeedValue));
            }
        });

        batteryDrone = (TextView) findViewById(R.id.battery_drone);

        batteryMyo = (TextView) findViewById(R.id.battery_myo);

        gestureCommand = (TextView) findViewById(R.id.gesture_command);

        emergencyBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                droneController.emergency();
                return false;
            }
        });

        emergencyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DroneActivity.this, getString(R.string.emergency), Toast.LENGTH_SHORT).show();
            }
        });

        startLandBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    starting = false;
                }

                switch (droneController.getFlyingState()) {
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED:
                        starting = event.getAction() == MotionEvent.ACTION_DOWN;
                        break;
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING:
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING:
                        starting = false;
                        droneController.land();
                        break;
                    default:
                }

                return false;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if (myoHub != null) {
            myoHub.setImuListener(this);
        }

        if (customGestureController != null) {
            customGestureController.addListener();
        }
        if (droneController != null) {
            droneController.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myoHub != null) {
            myoHub.removeImuListener(this);
        }
        if (customGestureController != null) {
            customGestureController.removeListener();
        }

        if (droneController != null) {
            droneController.removeListener(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // show a loading view while the bebop drone is connecting
        if ((droneController != null) && !(ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals(droneController.getConnectionState()))) {


            // if the connection to the Bebop fails, finish the activity
            if (!droneController.connect()) {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (droneController != null) {
            if (!droneController.disconnect()) {
                finish();
            }
        }
    }

    @Override
    public void onDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state) {


        if (state == ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING) {

            if (myoHub != null) {
                myoHub.setBatteryListener(this);
                myoHub.setCustomGestureListener(this, this);
                myoHub.setImuListener(this);
                customGestureController = myoHub.getCustomGestureController();
            } else {
                finish();
            }

            realm = RealmController.with(this).getRealm();
            droneModeDetection = new DroneModeDetection(handler);
            droneModeDetection.setListener(this);
        }
    }

    @Override
    public void onBatteryChargeChanged(final int batteryPercentage) {


        setText(batteryDrone, String.format(getString(R.string.percent_mask), batteryPercentage));


    }

    @Override
    public void onPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state) {

        if (state == ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING || state == ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING) {
            droneState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING;
            setText(startLandBtn, getString(R.string.tap_to_land_drone));
        } else {
            droneState = state;
        }

    }

    @Override
    public void onGestureDetected(long l, final Gestures.CustomGestures customGestures) {
        switch (customGestures) {
            case GESTURE_0:
                setImage(currentGesture, R.mipmap.solid_grey_fist);
                break;

            case GESTURE_1:
                setImage(currentGesture, R.mipmap.solid_grey_spread_fingers);
                break;

            case GESTURE_2:

                setImage(currentGesture, R.mipmap.solid_grey_wave_left);
                break;
            case GESTURE_3:

                setImage(currentGesture, R.mipmap.solid_grey_wave_right);
                break;
            case UNDEFINED:

                setImage(currentGesture, R.mipmap.solid_grey_unknown);
                break;
        }

    }

    @Override
    public void onGestureProfileLoaded(long l, Gestures.ProfileStatus profileStatus, int i) {

    }

    @Override
    public void onGestureRecordingStatusChanged(long l, Gestures.CustomGestures currentGesture, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onGestureStored(long l, Gestures.CustomGestures customGestures, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onCheckGestureDistance(long l, Gestures.CustomGestures customGestures, Gestures.CustomGestures customGestures1, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onBatteryLevelRead(MyoController myoController, MyoCommunicator myoCommunicator, int batteryLevel) {


        setText(batteryMyo, String.format(getString(R.string.percent_mask), batteryLevel));

    }

    @Override
    public void onOrientationChanged(final Orientation orientation) {
        Log.i(TAG, droneState.name());
        if (customGestureController != null && droneModeDetection != null) {
            droneModeDetection.getCurrentMode(customGestureController.getLastDetectedGesture(), droneState.ordinal(), orientation.getRollDegrees(), orientation.getPitchDegrees());
        }
    }

    @Override
    public void onAccelerationChanged(Accelerometer accelerometer) {

    }

    @Override
    public void onGyroscopeChanged(Gyroscope gyroscope) {

    }

    private void setText(final TextView textView, final String text) {
        if (handler != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(text);
                }
            });
        }
    }

    private void setText(final Button button, final String text) {
        if (handler != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    button.setText(text);
                }
            });
        }
    }

    @Override
    public void onModeDetected(final DroneControlCustomizer.DroneFlyingModes mode) {

        if (handler != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    gestureCommand.setText(mode.name());
                }
            });
        }

        if (isFeedback && lastFlyingMode != mode) {
            myoHub.vibrate(MyoCommands.VibrateType.SHORT);
            lastFlyingMode = mode;
        }


        if (droneController != null) {


            controlDrone(mode);

        }
    }

    private void controlDrone(DroneControlCustomizer.DroneFlyingModes mode) {

        if (droneState != ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING && mode != DroneControlCustomizer.DroneFlyingModes.START) {
            setText(startLandBtn, getString(R.string.perform_start_gesture));
        }

        if (droneState == ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING) {
            setText(startLandBtn, getString(R.string.tap_to_land_drone));
        }

        switch (mode) {
            case HOVER_MODE:
                droneController.flyInHoverMode();
                break;

            case FLY_FORWARD_MODE:
                droneController.flyForward(droneSpeedValue);
                break;

            case FLY_BACKWARD_MODE:
                droneController.flyBackward(droneSpeedValue);
                break;

            case FLY_UP_MODE:
                droneController.flyUp(droneSpeedValue);
                break;

            case FLY_DOWN_MODE:
                droneController.flyDown(droneSpeedValue);
                break;

            case FLY_RIGHT_MODE:
                droneController.flyRight(droneSpeedValue);
                break;

            case FLY_LEFT_MODE:
                droneController.flyLeft(droneSpeedValue);
                break;

            case ROTATE_RIGHT_MODE:
                droneController.rotateRight(droneSpeedValue);
                break;

            case ROTATE_LEFT_MODE:
                droneController.rotateLeft(droneSpeedValue);
                break;
            case STOP:
                droneController.land();
                break;
            case START:


                if (starting) {
                    droneController.takeOff();
                    setText(startLandBtn, getString(R.string.tap_to_land_drone));
                } else {
                    setText(startLandBtn, getString(R.string.tap_to_start_drone));
                }

                break;
            default:
                droneController.flyInHoverMode();
                break;
        }
    }

    private void setImage(final ImageView imageView, final int resource) {
        if (imageView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageResource(resource);
                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }
}
