package codes.zaak.myodrone2.model;




import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 16.06.16.
 */
public class DroneControlMode extends RealmObject {


    @PrimaryKey
    private int flyingModeName;

    private RealmList<Gesture> gestureList = new RealmList<>();

    private int droneState;

    private OrientationData orientationData;

    public int getFlyingModeName() {
        return flyingModeName;
    }

    public void setFlyingModeName(int flyingModeName) {
        this.flyingModeName = flyingModeName;
    }

    public int getDroneState() {
        return droneState;
    }

    public void setDroneState(int droneState) {
        this.droneState = droneState;
    }

    public RealmList<Gesture> getGestureList() {
        return gestureList;
    }

    public void setGestureList(RealmList<Gesture> gestureList) {
        this.gestureList = gestureList;
    }

    public OrientationData getOrientationData() {
        return orientationData;
    }


    public void setOrientationData(OrientationData orientationData) {
        this.orientationData = orientationData;
    }


    public String gestureListToString() {

        StringBuilder sb = new StringBuilder();

        for(Gesture gesture: gestureList) {
            sb.append(gesture.toString());
        }

        return sb.toString();
    }



}
