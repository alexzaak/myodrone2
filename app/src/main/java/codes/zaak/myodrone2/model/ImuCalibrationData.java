package codes.zaak.myodrone2.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexander Zaak on 19.06.16.
 */
public class ImuCalibrationData extends RealmObject {

    @PrimaryKey
    private int id;

    private double roll;
    private double pitch;
    private double yaw;

    private double minRoll;
    private double maxRoll;

    private double minPitch;
    private double maxPitch;

    private double minYaw;
    private double maxYaw;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRoll() {
        return roll;
    }

    public void setRoll(double roll) {
        this.roll = roll;
    }

    public double getPitch() {
        return pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    public double getYaw() {
        return yaw;
    }

    public void setYaw(double yaw) {
        this.yaw = yaw;
    }

    public double getMinRoll() {
        return minRoll;
    }

    public void setMinRoll(double minRoll) {
        this.minRoll = minRoll;
    }

    public double getMaxRoll() {
        return maxRoll;
    }

    public void setMaxRoll(double maxRoll) {
        this.maxRoll = maxRoll;
    }

    public double getMinPitch() {
        return minPitch;
    }

    public void setMinPitch(double minPitch) {
        this.minPitch = minPitch;
    }

    public double getMaxPitch() {
        return maxPitch;
    }

    public void setMaxPitch(double maxPitch) {
        this.maxPitch = maxPitch;
    }

    public double getMinYaw() {
        return minYaw;
    }

    public void setMinYaw(double minYaw) {
        this.minYaw = minYaw;
    }

    public double getMaxYaw() {
        return maxYaw;
    }

    public void setMaxYaw(double maxYaw) {
        this.maxYaw = maxYaw;
    }
}
