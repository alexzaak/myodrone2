package codes.zaak.myodrone2.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexander Zaak on 16.06.16.
 */
public class OrientationData extends RealmObject {

    @PrimaryKey
    private int id;

    private double minRollRight;
    private double maxRollRight;

    private double minRollLeft;
    private double maxRollLeft;

    private double minPitchUp;
    private double maxPitchUp;


    private double minPitchDown;
    private double maxPitchDown;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMinRollRight() {
        return minRollRight;
    }

    public void setMinRollRight(double minRollRight) {
        this.minRollRight = minRollRight;
    }

    public double getMaxRollRight() {
        return maxRollRight;
    }

    public void setMaxRollRight(double maxRollRight) {
        this.maxRollRight = maxRollRight;
    }

    public double getMinRollLeft() {
        return minRollLeft;
    }

    public void setMinRollLeft(double minRollLeft) {
        this.minRollLeft = minRollLeft;
    }

    public double getMaxRollLeft() {
        return maxRollLeft;
    }

    public void setMaxRollLeft(double maxRollLeft) {
        this.maxRollLeft = maxRollLeft;
    }

    public double getMinPitchUp() {
        return minPitchUp;
    }

    public void setMinPitchUp(double minPitchUp) {
        this.minPitchUp = minPitchUp;
    }

    public double getMaxPitchUp() {
        return maxPitchUp;
    }

    public void setMaxPitchUp(double maxPitchUp) {
        this.maxPitchUp = maxPitchUp;
    }

    public double getMinPitchDown() {
        return minPitchDown;
    }

    public void setMinPitchDown(double minPitchDown) {
        this.minPitchDown = minPitchDown;
    }

    public double getMaxPitchDown() {
        return maxPitchDown;
    }

    public void setMaxPitchDown(double maxPitchDown) {
        this.maxPitchDown = maxPitchDown;
    }
}
