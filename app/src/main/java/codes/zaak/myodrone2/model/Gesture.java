package codes.zaak.myodrone2.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexander Zaak on 18.06.16.
 */
public class Gesture extends RealmObject {

    @PrimaryKey
    private int gestureId;


    private String gestureName;


    public String getGestureName() {
        return gestureName;
    }

    public void setGestureName(String gestureName) {
        this.gestureName = gestureName;
    }

    public int getGestureId() {
        return gestureId;
    }

    public void setGestureId(int gestureId) {
        this.gestureId = gestureId;
    }

    @Override
    public String toString() {
        return "\n[ " +
                "\ngestureId=" + gestureId +
                "\ngestureName='" + gestureName + " ]";
    }
}
