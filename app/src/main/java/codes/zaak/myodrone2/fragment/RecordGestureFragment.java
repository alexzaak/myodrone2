package codes.zaak.myodrone2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import codes.zaak.myodrone2.CalibrationActivity;
import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.controller.RealmController;
import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myodrone2.model.Gesture;
import codes.zaak.myodrone2.model.OrientationData;
import codes.zaak.myodrone2.utils.PrefManager;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myodrone2.utils.UiInteractionStates;
import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.gestures.CustomGestureController;
import io.realm.Realm;

/**
 * Created by Alexander Zaak on 10.06.16.
 * A placeholder fragment containing a simple view.
 */
public class RecordGestureFragment extends Fragment implements MyoListener.CustomGesturesListener {

    private static final String TAG = RecordGestureFragment.class.getName();
    private CustomGestureController customGestureController;

    private Handler handler;

    private TextView gestureStatus;

    private ImageView imageViewDetectedGesture;

    private UiInteractionListener.FragmentListener fragmentListener;

    private Realm realm;

    private PrefManager prefManager;

    private MyoHub myoHub;

    public RecordGestureFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = RealmController.with(getActivity()).getRealm();
        handler = new Handler();
        prefManager = new PrefManager(getActivity());
        myoHub = App.getMyoHub();
        if (myoHub != null) {
            myoHub.setCustomGestureListener(getActivity(), this);

            customGestureController = myoHub.getCustomGestureController();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        fragmentListener = (CalibrationActivity) context;


    }

    @Override
    public void onResume() {
        super.onResume();


        if (customGestureController != null) {
            customGestureController.addListener();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (customGestureController != null) {
            customGestureController.removeListener();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calibration, container, false);

        Button btnRecGesture0 = (Button) view.findViewById(R.id.rec_btn_gesture_0);
        Button btnRecGesture1 = (Button) view.findViewById(R.id.rec_btn_gesture_1);
        Button btnRecGesture2 = (Button) view.findViewById(R.id.rec_btn_gesture_2);
        Button btnRecGesture3 = (Button) view.findViewById(R.id.rec_btn_gesture_3);

        Button btnNext = (Button) view.findViewById(R.id.btn_next);

        Button btnRecGestureClearAll = (Button) view.findViewById(R.id.btn_gesture_clear_all);

        gestureStatus = (TextView) view.findViewById(R.id.text_rec_gesture_status);

        imageViewDetectedGesture = (ImageView) view.findViewById(R.id.image_detected_gesture);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListener.onNextClicked(UiInteractionStates.Pages.IMU_CALIBRATION);
            }
        });

        btnRecGesture0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(Gestures.CustomGestures.GESTURE_0, MyoStates.GestureStatus.RECORDING);
                storeGesture(Gestures.CustomGestures.GESTURE_0);

            }
        });

        btnRecGesture1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(Gestures.CustomGestures.GESTURE_1, MyoStates.GestureStatus.RECORDING);
                storeGesture(Gestures.CustomGestures.GESTURE_1);
            }
        });

        btnRecGesture2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(Gestures.CustomGestures.GESTURE_2, MyoStates.GestureStatus.RECORDING);
                storeGesture(Gestures.CustomGestures.GESTURE_2);
            }
        });

        btnRecGesture3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(Gestures.CustomGestures.GESTURE_3, MyoStates.GestureStatus.RECORDING);
                storeGesture(Gestures.CustomGestures.GESTURE_3);
            }
        });

        btnRecGestureClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(null, MyoStates.GestureStatus.CLEAR);
                clearGesture();
            }
        });

        return view;
    }

    private void clearGesture() {
        realm.beginTransaction();
        realm.delete(Gesture.class);
        realm.delete(DroneControlMode.class);
        realm.delete(OrientationData.class);
        realm.commitTransaction();
    }

    private void storeGesture(Gestures.CustomGestures gestureToStore) {

        Gesture gesture = new Gesture();
        gesture.setGestureId(gestureToStore.ordinal());
        gesture.setGestureName(gestureToStore.name());

        // Persist your data easily
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(gesture);
        realm.commitTransaction();

    }

    @Override
    public void onGestureDetected(long l, Gestures.CustomGestures customGestures) {
        switch (customGestures) {
            case GESTURE_0:
                setImage(imageViewDetectedGesture, R.mipmap.solid_grey_fist);
                break;

            case GESTURE_1:
                setImage(imageViewDetectedGesture, R.mipmap.solid_grey_spread_fingers);
                break;

            case GESTURE_2:

                setImage(imageViewDetectedGesture, R.mipmap.solid_grey_wave_left);
                break;
            case GESTURE_3:

                setImage(imageViewDetectedGesture, R.mipmap.solid_grey_wave_right);
                break;
            case UNDEFINED:

                setImage(imageViewDetectedGesture, R.mipmap.solid_grey_unknown);
                break;
        }
    }

    @Override
    public void onGestureProfileLoaded(long l, Gestures.ProfileStatus profileStatus, int i) {

    }

    @Override
    public void onGestureRecordingStatusChanged(long l, Gestures.CustomGestures currentGesture, Gestures.GestureSaveStatus gestureSaveStatus) {
        setText(gestureStatus, gestureSaveStatus.name());
    }

    @Override
    public void onGestureStored(long l, Gestures.CustomGestures customGestures, Gestures.GestureSaveStatus gestureSaveStatus) {
        Log.i(TAG, String.format("Gesture: %s Status: %s", customGestures.name(), gestureSaveStatus.name()));
        if (gestureSaveStatus == Gestures.GestureSaveStatus.SUCCESS || gestureSaveStatus == Gestures.GestureSaveStatus.ERROR) {
            customGestureController.setGestureStatus(null, MyoStates.GestureStatus.DETECTING);
        }
    }

    @Override
    public void onCheckGestureDistance(long l, Gestures.CustomGestures customGestures, Gestures.CustomGestures customGestures1, Gestures.GestureSaveStatus gestureSaveStatus) {
        setText(gestureStatus, getString(R.string.to_simular));
    }

    private void setText(final TextView textView, final String text) {

        if (textView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(text);
                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }

    private void setImage(final ImageView imageView, final int resource) {
        if (imageView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageResource(resource);
                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }
}
