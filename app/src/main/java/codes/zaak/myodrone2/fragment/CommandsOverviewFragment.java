package codes.zaak.myodrone2.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;

import codes.zaak.myodrone2.CalibrationActivity;
import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.controller.RealmController;
import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myodrone2.model.Gesture;
import codes.zaak.myodrone2.model.ImuCalibrationData;
import codes.zaak.myodrone2.model.OrientationData;
import codes.zaak.myodrone2.utils.DroneControlCustomizer;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myodrone2.utils.UiInteractionStates;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 * Created by Alexander Zaak on 10.06.16.
 *
 * A simple {@link Fragment} subclass.
 */
public class CommandsOverviewFragment extends Fragment {

    private static final String TAG = CommandsOverviewFragment.class.getName();

    private static final String TAG_RIGHT_SEEK = "roll_right";
    private static final String TAG_LEFT_SEEK = "roll_left";
    private static final String TAG_UP_SEEK = "pitch_up";
    private static final String TAG_DOWN_SEEK = "pitch_down";


    private UiInteractionListener.FragmentListener mListener;

    private Realm realm;
    private RealmController realmController;

    private DroneControlMode controlMode;


    private ImuCalibrationData imuCalibrationData;

    private RealmResults<Gesture> gestureList;


    protected static final DroneControlCustomizer.DroneFlyingModes HOVER_MODE = DroneControlCustomizer.DroneFlyingModes.HOVER_MODE;

    public CommandsOverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (CalibrationActivity) context;


    }

    @Override
    public void onDetach() {
        super.onDetach();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = RealmController.with(getActivity()).getRealm();
        realmController = RealmController.getInstance();


        generateRemoteControl();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hover, container, false);

        Button btnNext = (Button) view.findViewById(R.id.btn_next);
        Button btnBack = (Button) view.findViewById(R.id.btn_back);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNextClicked(UiInteractionStates.Pages.CALIBRATION_TEST);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onBackClicked(UiInteractionStates.Pages.IMU_CALIBRATION);
            }
        });
        return view;
    }


    private double getDefaultValuePitch(double min, double max) {
        return (max - min) / 4;
    }

    private double getDefaultValueRoll(double min, double max) {
        return (max - min) / 2;
    }


    private double getLastFifthDefaultValue(double min, double max) {
        return (min + max) - ((min + max) / 6);
    }


    private void generateRemoteControl() {


        realm.executeTransactionAsync(new Realm.Transaction() {


            @Override
            public void execute(Realm realm) {
                imuCalibrationData = realm.where(ImuCalibrationData.class).findFirst();
                Log.i(TAG, imuCalibrationData.toString());

                gestureList = realm.where(Gesture.class).findAll();

                int droneFlyState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING.ordinal();
                int droneLandedState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED.ordinal();
                int droneHoverState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING.ordinal();

                double rollRightValue = getDefaultValueRoll(imuCalibrationData.getRoll(), imuCalibrationData.getMaxRoll());
                double rollLeftValue = getDefaultValueRoll(imuCalibrationData.getRoll(), imuCalibrationData.getMinRoll());
                double pitchUpValue = getDefaultValuePitch(imuCalibrationData.getPitch(), imuCalibrationData.getMaxPitch());
                double pitchDownValue = getDefaultValuePitch(imuCalibrationData.getPitch(), imuCalibrationData.getMinPitch());

                double lastFifthValue = getLastFifthDefaultValue(imuCalibrationData.getPitch(), imuCalibrationData.getMinPitch());

                realm.delete(DroneControlMode.class);
                realm.delete(OrientationData.class);

                for (DroneControlCustomizer.DroneFlyingModes mode : DroneControlCustomizer.DroneFlyingModes.values()) {

                    DroneControlMode controlMode = realm.createObject(DroneControlMode.class, mode.ordinal());
                    controlMode.setDroneState(droneFlyState);

                    OrientationData orientationData = realm.createObject(OrientationData.class, mode.ordinal());

                    orientationData.setMinRollRight(imuCalibrationData.getRoll());
                    orientationData.setMaxRollRight(rollRightValue);

                    orientationData.setMinRollLeft(imuCalibrationData.getRoll());
                    orientationData.setMaxRollLeft(rollLeftValue);


                    orientationData.setMinPitchUp(imuCalibrationData.getPitch());
                    orientationData.setMaxPitchUp(pitchUpValue);

                    orientationData.setMinPitchDown(imuCalibrationData.getPitch());
                    orientationData.setMaxPitchDown(pitchDownValue);


                    switch (mode) {
                        case HOVER_MODE:
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(1));
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(2));
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(3));

                            break;

                        case STOP:
                            orientationData.setMaxPitchUp(pitchDownValue * 3);
                            orientationData.setMaxPitchDown(imuCalibrationData.getMinPitch() * 5);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(3));
                            break;
                        case FLY_FORWARD_MODE:

                            orientationData.setMaxRollLeft(rollLeftValue);
                            orientationData.setMaxRollRight(rollRightValue);

                            orientationData.setMaxPitchDown(pitchUpValue);
                            orientationData.setMaxPitchUp(imuCalibrationData.getMaxPitch() * 4);

                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            break;

                        case FLY_BACKWARD_MODE:
                            orientationData.setMaxRollLeft(rollLeftValue);
                            orientationData.setMaxRollRight(rollRightValue);

                            orientationData.setMaxPitchUp(pitchDownValue);
                            orientationData.setMaxPitchDown(imuCalibrationData.getMinPitch() * 4);

                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            break;

                        case FLY_RIGHT_MODE:
                            orientationData.setMaxPitchDown(pitchDownValue * 2);
                            orientationData.setMaxPitchUp(pitchUpValue * 2);

                            orientationData.setMaxRollLeft(rollRightValue);
                            orientationData.setMaxRollRight(imuCalibrationData.getMaxRoll() * 2);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            break;

                        case FLY_LEFT_MODE:
                            orientationData.setMaxPitchDown(pitchDownValue * 2);
                            orientationData.setMaxPitchUp(pitchUpValue * 2);

                            orientationData.setMaxRollRight(rollLeftValue);
                            orientationData.setMaxRollLeft(imuCalibrationData.getMinRoll() * 2);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            break;

                        case START:
                            controlMode.setDroneState(droneLandedState);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.first());
                            break;


                        case FLY_UP_MODE:
                            orientationData.setMaxRollLeft(rollLeftValue);
                            orientationData.setMaxRollRight(rollRightValue);

                            orientationData.setMaxPitchDown(pitchUpValue);
                            orientationData.setMaxPitchUp(imuCalibrationData.getMaxPitch() * 4);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(1));
                            break;

                        case FLY_DOWN_MODE:
                            orientationData.setMaxRollLeft(rollLeftValue);
                            orientationData.setMaxRollRight(rollRightValue);

                            orientationData.setMaxPitchUp(pitchDownValue);
                            orientationData.setMaxPitchDown(imuCalibrationData.getMinPitch() * 4);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(1));
                            break;

                        case ROTATE_RIGHT_MODE:
                            orientationData.setMinRollLeft(imuCalibrationData.getRoll() * 1.5);
                            orientationData.setMaxRollLeft(imuCalibrationData.getRoll() * 1.5);

                            orientationData.setMaxRollRight(imuCalibrationData.getMaxRoll() * 4);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(3));
                            break;


                        case ROTATE_LEFT_MODE:
                            orientationData.setMinRollLeft(imuCalibrationData.getRoll() * 1.5);
                            orientationData.setMaxRollLeft(imuCalibrationData.getRoll() * 1.5);

                            orientationData.setMaxRollRight(imuCalibrationData.getMaxRoll() * 4);
                            controlMode.getGestureList().add(CommandsOverviewFragment.this.gestureList.get(2));
                            break;

                    }

                    controlMode.setOrientationData(orientationData);

                    realm.copyToRealmOrUpdate(controlMode);
                }
                // Toast.makeText(CommandsOverviewFragment.this.getActivity(), "Finish", Toast.LENGTH_SHORT).show();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "finish");

                RealmResults<DroneControlMode> list = realm.where(DroneControlMode.class).findAll();

                for (DroneControlMode mode : list) {
                    Log.i(TAG, "\n\n" + DroneControlCustomizer.getModes(mode.getFlyingModeName()).name());
                    Log.i(TAG, mode.getOrientationData().toString());
                    Log.i(TAG, mode.gestureListToString());
                }
            }
        });


    }
}
