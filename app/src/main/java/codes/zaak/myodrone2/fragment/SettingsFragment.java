package codes.zaak.myodrone2.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.utils.Const;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoStates;

/**
 * Created by Alexander Zaak on 12.05.15.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    private static final String TAG = SettingsFragment.class.getName();
    private MyoHub myoHub;


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        myoHub = App.getMyoHub();
    }

    @Override
    public void onResume() {
        super.onResume();
        myoHub = App.getMyoHub();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
if(myoHub==null) {

    Toast.makeText(getActivity(), getString(R.string.connect_to_myo), Toast.LENGTH_SHORT).show();

    return;
}
        String feedback = getString(R.string.pref_key_feedback);
        Log.i(TAG, key);
        switch (key) {

            case Const.PREF_FEEDBACK:

                break;

            case Const.PREF_AUTOMATIC_REFINE:

                break;

            case Const.PREF_ALGORITHM:

                String type = prefs.getString(Const.PREF_ALGORITHM, null);
                if (type != null) {
                    int algType = Integer.valueOf(type);
                    if (algType == 0) {

                        myoHub.setAlgorithmType(getActivity(), MyoStates.AlgorithmType.TWO_VECTOR_DISTANCE);
                    }

                    if (algType == 1) {
                        myoHub.setAlgorithmType(getActivity(), MyoStates.AlgorithmType.MATHEMATICAL_SINUS_DISTANCE);
                    }
                    if (algType == 2) {
                        myoHub.setAlgorithmType(getActivity(), MyoStates.AlgorithmType.MATHEMATICAL_COSINUS_DISTANCE);
                    }

                }

                break;


        }
    }
}
