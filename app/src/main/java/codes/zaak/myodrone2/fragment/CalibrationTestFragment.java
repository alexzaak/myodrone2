package codes.zaak.myodrone2.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;

import org.w3c.dom.Text;

import java.util.List;

import codes.zaak.myodrone2.CalibrationActivity;
import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.controller.RealmController;
import codes.zaak.myodrone2.model.DroneControlMode;
import codes.zaak.myodrone2.utils.DroneControlCustomizer;
import codes.zaak.myodrone2.utils.DroneModeDetection;
import codes.zaak.myodrone2.utils.PrefManager;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myodrone2.utils.UiInteractionStates;
import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.commands.MyoCommands;
import codes.zaak.myorecognizer.gestures.CustomGestureController;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Orientation;
import io.realm.Realm;

/**
 * Created by Alexander Zaak on 10.06.16.
 * A simple {@link Fragment} subclass.
 */
public class CalibrationTestFragment extends Fragment implements MyoListener.ImuDataListener, MyoListener.CustomGesturesListener, DroneModeDetection.Listener {

    private static final String TAG = CalibrationTestFragment.class.getName();
    private TextView testDroneMode;

    private ImageView currentGesture;

    private Realm realm;

    private DroneModeDetection droneModeDetection;

    private UiInteractionListener.FragmentListener fragmentListener;

    private CustomGestureController customGestureController;

    private DroneControlCustomizer.DroneFlyingModes lastFlyingMode = DroneControlCustomizer.DroneFlyingModes.UNDEFINED;

    private ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM droneState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED;

    private Handler handler;

    private MyoHub myoHub = App.getMyoHub();

    private PrefManager prefManager;

    public CalibrationTestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        prefManager = new PrefManager(getActivity());
        if (myoHub != null) {
            myoHub.setImuListener(this);
            myoHub.setCustomGestureListener(getActivity(), this);
            customGestureController = myoHub.getCustomGestureController();

            myoHub.setImuListener(this);
        }

        realm = RealmController.with(getActivity()).getRealm();
        droneModeDetection = new DroneModeDetection(handler);
        droneModeDetection.setListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        fragmentListener = (CalibrationActivity) context;


    }

    @Override
    public void onResume() {
        super.onResume();
        if (myoHub != null) {
            myoHub.setImuListener(this);
        }

        if(customGestureController !=null) {
            customGestureController.addListener();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myoHub != null) {
            myoHub.removeImuListener(this);
        }
        if(customGestureController !=null) {
            customGestureController.removeListener();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calibration_test, container, false);

        Button btnBack = (Button) view.findViewById(R.id.btn_back);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentListener.onBackClicked(UiInteractionStates.Pages.DRONE_CONTROL_SETUP);
            }
        });

        List<DroneControlMode> droneControlModeList = realm.where(DroneControlMode.class).findAll();


        if (droneControlModeList != null) {
            Log.i(TAG, "SIZE " + droneControlModeList.size());
            for (DroneControlMode mode : droneControlModeList) {
                String modeName = DroneControlCustomizer.getModes(mode.getFlyingModeName()).name();
                Log.i(TAG, "MODE " + modeName);

                Log.i(TAG, "GESTURES " + mode.gestureListToString() + "size: " + mode.getGestureList().size());
            }


        }
        Switch droneStateSwitch = (Switch) view.findViewById(R.id.btn_drone_state);

        droneStateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    droneState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING;

                } else {
                    droneState = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED;
                }
            }
        });

        testDroneMode = (TextView) view.findViewById(R.id.textview_drone_mode_test);
        currentGesture = (ImageView) view.findViewById(R.id.current_gesture);


        return view;
    }

    @Override
    public void onGestureDetected(long l, Gestures.CustomGestures customGestures) {
        switch (customGestures) {
            case GESTURE_0:
                setImage(currentGesture, R.mipmap.solid_grey_fist);
                break;

            case GESTURE_1:
                setImage(currentGesture, R.mipmap.solid_grey_spread_fingers);
                break;

            case GESTURE_2:

                setImage(currentGesture, R.mipmap.solid_grey_wave_left);
                break;
            case GESTURE_3:

                setImage(currentGesture, R.mipmap.solid_grey_wave_right);
                break;
            case UNDEFINED:

                setImage(currentGesture, R.mipmap.solid_grey_unknown);
                break;
        }
    }

    @Override
    public void onGestureProfileLoaded(long l, Gestures.ProfileStatus profileStatus, int i) {

    }

    @Override
    public void onGestureRecordingStatusChanged(long l, Gestures.CustomGestures currentGesture, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onGestureStored(long l, Gestures.CustomGestures customGestures, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onCheckGestureDistance(long l, Gestures.CustomGestures customGestures, Gestures.CustomGestures customGestures1, Gestures.GestureSaveStatus gestureSaveStatus) {

    }

    @Override
    public void onOrientationChanged(Orientation orientation) {
        if (customGestureController != null && droneModeDetection != null) {
            droneModeDetection.getCurrentMode(customGestureController.getLastDetectedGesture(), droneState.ordinal(), orientation.getRollDegrees(), orientation.getPitchDegrees());

        }
    }

    @Override
    public void onAccelerationChanged(Accelerometer accelerometer) {

    }

    @Override
    public void onGyroscopeChanged(Gyroscope gyroscope) {

    }

    private void setText(final TextView textView, final String text) {

        if (textView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(text);
                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }

    @Override
    public void onModeDetected(DroneControlCustomizer.DroneFlyingModes mode) {


        if (prefManager.isFeedBack() && lastFlyingMode != mode) {
            myoHub.vibrate(MyoCommands.VibrateType.SHORT);
            lastFlyingMode = mode;
        }

        setText(testDroneMode, mode.name());
    }

    private void setImage(final ImageView imageView, final int resource) {
        if (imageView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageResource(resource);
                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }
}
