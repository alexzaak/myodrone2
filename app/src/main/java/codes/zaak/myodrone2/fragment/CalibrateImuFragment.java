package codes.zaak.myodrone2.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import codes.zaak.myodrone2.CalibrationActivity;
import codes.zaak.myodrone2.R;
import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.controller.RealmController;
import codes.zaak.myodrone2.model.Gesture;
import codes.zaak.myodrone2.model.ImuCalibrationData;
import codes.zaak.myodrone2.utils.UiInteractionListener;
import codes.zaak.myodrone2.utils.UiInteractionStates;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Orientation;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Alexander Zaak on 10.06.16.
 */

public class CalibrateImuFragment extends Fragment implements MyoListener.ImuDataListener {

    private static final String TAG = CalibrationTestFragment.class.getName();

    public enum ImuData {
        ROLL_RIGHT,
        ROLL_LEFT,
        PITCH_UP,
        PITCH_DOWN,
        MIDDLE,

        ROLL,
        PITCH,
        UNDEFINED
    }

    private UiInteractionListener.FragmentListener mListener;
    private Realm realm;


    private Button btnRollRight;
    private Button btnRollLeft;
    private Button btnPitchUp;
    private Button btnPitchDown;
    private Button btnMiddle;


    private TextView textRollRight;
    private TextView textRollLeft;
    private TextView textPitchUp;
    private TextView textPitchDown;
    private TextView textMiddle;

    private Orientation orientation;

    private Handler handler;

    private MyoHub myoHub;

    private ImuData currentRecordingImuData = ImuData.UNDEFINED;

    public CalibrateImuFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        myoHub = App.getMyoHub();
        if (myoHub != null) {
            myoHub.setImuListener(this);
        }

        realm = RealmController.with(getActivity()).getRealm();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calibrate_imu, container, false);

        Button btnNext = (Button) view.findViewById(R.id.btn_next);
        Button btnBack = (Button) view.findViewById(R.id.btn_back);


        btnRollRight = (Button) view.findViewById(R.id.btn_calibrate_roll_right);
        btnRollLeft = (Button) view.findViewById(R.id.btn_calibrate_roll_left);
        btnPitchUp = (Button) view.findViewById(R.id.btn_calibrate_pitch_up);
        btnPitchDown = (Button) view.findViewById(R.id.btn_calibrate_pitch_down);
        btnMiddle = (Button) view.findViewById(R.id.btn_calibrate_middle);

        btnRollRight.setPressed(false);
        btnRollLeft.setPressed(false);
        btnPitchUp.setPressed(false);
        btnPitchDown.setPressed(false);
        btnMiddle.setPressed(false);

        textRollRight = (TextView) view.findViewById(R.id.text_calibrate_roll_right);
        textRollLeft = (TextView) view.findViewById(R.id.text_calibrate_roll_left);
        textPitchUp = (TextView) view.findViewById(R.id.text_calibrate_pitch_up);
        textPitchDown = (TextView) view.findViewById(R.id.text_calibrate_pitch_down);
        textMiddle = (TextView) view.findViewById(R.id.text_calibrate_middle);


        ImuCalibrationData data = realm.where(ImuCalibrationData.class).equalTo("id", 1).findFirst();
        RealmResults<ImuCalibrationData> dataList = realm.where(ImuCalibrationData.class).findAll();
        Log.i("IMU", "Size: " + dataList.size());
        for (ImuCalibrationData item : dataList) {
            Log.i("IMU", "Id: " + item.getId());
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNextClicked(UiInteractionStates.Pages.DRONE_CONTROL_SETUP);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onBackClicked(UiInteractionStates.Pages.GESTURE_CALIBRATION);
            }
        });

        if (data != null) {
            setText(textRollRight, data, ImuData.ROLL_RIGHT);
            setText(textRollLeft, data, ImuData.ROLL_LEFT);
            setText(textPitchUp, data, ImuData.PITCH_UP);
            setText(textPitchDown, data, ImuData.PITCH_DOWN);
            setText(textMiddle, data, ImuData.MIDDLE);

        }

        btnRollRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentRecordingImuData = ImuData.ROLL_RIGHT;
                        break;

                    case MotionEvent.ACTION_UP:
                        currentRecordingImuData = ImuData.UNDEFINED;
                        storeImuData(ImuData.ROLL_RIGHT);
                        break;
                }

                return false;
            }
        });


        btnRollLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentRecordingImuData = ImuData.ROLL_LEFT;
                        break;

                    case MotionEvent.ACTION_UP:
                        currentRecordingImuData = ImuData.UNDEFINED;
                        storeImuData(ImuData.ROLL_LEFT);
                        break;
                }

                return false;
            }
        });


        btnPitchUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentRecordingImuData = ImuData.PITCH_UP;
                        break;

                    case MotionEvent.ACTION_UP:
                        currentRecordingImuData = ImuData.UNDEFINED;
                        storeImuData(ImuData.PITCH_UP);
                        break;
                }

                return false;
            }
        });


        btnPitchDown.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentRecordingImuData = ImuData.PITCH_DOWN;
                        break;

                    case MotionEvent.ACTION_UP:
                        currentRecordingImuData = ImuData.UNDEFINED;
                        storeImuData(ImuData.PITCH_DOWN);
                        break;
                }

                return false;
            }
        });


        btnMiddle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        currentRecordingImuData = ImuData.MIDDLE;
                        break;

                    case MotionEvent.ACTION_UP:
                        currentRecordingImuData = ImuData.UNDEFINED;
                        storeImuData(ImuData.MIDDLE);
                        break;
                }

                return false;
            }
        });


        return view;
    }

    private void storeImuData(ImuData imuData) {

        ImuCalibrationData imuCalibrationData = realm.where(ImuCalibrationData.class).equalTo("id", 1).findFirst();

        if (imuCalibrationData == null) {
            imuCalibrationData = new ImuCalibrationData();
            imuCalibrationData.setId(1);
        }

        // Persist your data easily
        realm.beginTransaction();
        switch (imuData) {

            case ROLL_RIGHT:
                imuCalibrationData.setMaxRoll(orientation.getRollDegrees());
                break;

            case ROLL_LEFT:
                imuCalibrationData.setMinRoll(orientation.getRollDegrees());
                break;

            case PITCH_UP:
                imuCalibrationData.setMaxPitch(orientation.getPitchDegrees());
                break;

            case PITCH_DOWN:
                imuCalibrationData.setMinPitch(orientation.getPitchDegrees());
                break;

            case MIDDLE:
                imuCalibrationData.setRoll(orientation.getRollDegrees());
                imuCalibrationData.setPitch(orientation.getPitchDegrees());
                break;

            default:
                return;
        }


        realm.copyToRealmOrUpdate(imuCalibrationData);
        realm.commitTransaction();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mListener = (CalibrationActivity) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (myoHub != null) {
            myoHub.setImuListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myoHub != null) {
            myoHub.removeImuListener(this);
        }
    }

    @Override
    public void onOrientationChanged(Orientation orientation) {
        this.orientation = orientation;
        setText();
    }

    @Override
    public void onAccelerationChanged(Accelerometer accelerometer) {

    }

    @Override
    public void onGyroscopeChanged(Gyroscope gyroscope) {

    }

    private void setText() {
        if (orientation == null) {
            return;
        }


            handler.post(new Runnable() {
                @Override
                public void run() {
                    String value;

                    switch (currentRecordingImuData) {
                        case ROLL_RIGHT:
                            value = String.format(Locale.GERMANY, "%.2f", orientation.getRollDegrees());
                            textRollRight.setText(value);
                            break;
                        case ROLL_LEFT:
                            value = String.format(Locale.GERMANY, "%.2f", orientation.getRollDegrees());
                            textRollLeft.setText(value);
                            break;
                        case PITCH_UP:
                            value = String.format(Locale.GERMANY, "%.2f", orientation.getPitchDegrees());
                            textPitchUp.setText(value);
                            break;
                        case PITCH_DOWN:
                            value = String.format(Locale.GERMANY, "%.2f", orientation.getPitchDegrees());
                            textPitchDown.setText(value);
                            break;
                        case MIDDLE:
                            value = String.format(Locale.GERMANY, "%.2f / %.2f", orientation.getRollDegrees(), orientation.getPitchDegrees());
                            textMiddle.setText(value);
                            break;
                        default:
                            value = "";
                            break;
                    }



                }
            });

    }

    private void setText(final TextView textView, final ImuCalibrationData imuData, final ImuData angle) {


        if (textView != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    String value;

                    switch (angle) {
                        case ROLL_RIGHT:
                            value = String.format(Locale.GERMANY, "%.2f", imuData.getMaxRoll());
                            break;

                        case ROLL_LEFT:
                            value = String.format(Locale.GERMANY, "%.2f", imuData.getMinRoll());
                            break;
                        case PITCH_UP:
                            value = String.format(Locale.GERMANY, "%.2f", imuData.getMaxPitch());
                            break;

                        case PITCH_DOWN:
                            value = String.format(Locale.GERMANY, "%.2f", imuData.getMinPitch());
                            break;
                        case MIDDLE:
                            value = String.format(Locale.GERMANY, "%.2f / %.2f", imuData.getRoll(), imuData.getPitch());
                            break;
                        default:
                            value = "";
                            break;
                    }
                    textView.setText(value);


                }
            });
        } else {
            Log.i("Adapter", "TextView null");
        }
    }
}
