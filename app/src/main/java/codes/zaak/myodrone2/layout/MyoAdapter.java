package codes.zaak.myodrone2.layout;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import codes.zaak.myodrone2.R;
import codes.zaak.myorecognizer.MyoController;

/**
 * Created by Alexander Zaak on 09.06.16.
 */
public class MyoAdapter extends RecyclerView.Adapter<MyoAdapter.MyoViewHolder> {

    private List<MyoController> myoControllerList;

    public MyoAdapter(List<MyoController> myoControllerList) {
        this.myoControllerList = myoControllerList;
    }

    @Override
    public MyoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myo_list_row, parent, false);

        return new MyoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyoViewHolder holder, int position) {
        MyoController myoController = myoControllerList.get(position);
        holder.name.setText(myoController.getBluetoothDevice().getName());
    }

    @Override
    public int getItemCount() {
        return myoControllerList.size();
    }

    public class MyoViewHolder extends RecyclerView.ViewHolder {
        public TextView name, status;

        public MyoViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            status = (TextView) view.findViewById(R.id.status);
        }
    }
}
