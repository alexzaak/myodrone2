package codes.zaak.myodrone2.layout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;

import java.util.List;

import codes.zaak.myodrone2.R;
import codes.zaak.myorecognizer.MyoController;

/**
 * Created by Alexander Zaak on 09.06.16.
 */
public class DroneAdapter extends RecyclerView.Adapter<DroneAdapter.DroneViewHolder> {

    private List<ARDiscoveryDeviceService> droneServiceList;

    public DroneAdapter(List<ARDiscoveryDeviceService> droneServiceList) {
        this.droneServiceList = droneServiceList;
    }

    @Override
    public DroneViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myo_list_row, parent, false);

        return new DroneViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DroneViewHolder holder, int position) {
        ARDiscoveryDeviceService droneService = droneServiceList.get(position);
        holder.name.setText(droneService.getName());
    }

    @Override
    public int getItemCount() {
        return droneServiceList.size();
    }

    public class DroneViewHolder extends RecyclerView.ViewHolder {
        public TextView name, status;

        public DroneViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            status = (TextView) view.findViewById(R.id.status);
        }
    }
}