package codes.zaak.myodrone2;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;

import java.util.List;

import codes.zaak.myodrone2.controller.App;
import codes.zaak.myodrone2.drone.DroneDiscover;
import codes.zaak.myodrone2.drone.DroneListener;
import codes.zaak.myodrone2.utils.Const;
import codes.zaak.myorecognizer.MyoGattCallback;
import codes.zaak.myorecognizer.MyoHub;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;

/**
 * Created by Alexander Zaak on 18.06.16.
 */
public class DeviceActivity extends AppCompatActivity implements DroneListener.DiscoverListener, MyoListener.ConnectionListener {

    public static final String EXTRA_DEVICE_SERVICE = "EXTRA_DEVICE_SERVICE";
    private static final String TAG = DeviceActivity.class.getName();

    // this block loads the native libraries
    // it is mandatory
    static {
        ARSDK.loadSDKLibs();
    }

    public DroneDiscover mDroneDiscoverer;
    private MyoHub myoHub;
    private ImageView myoIcon;
    private ImageView droneIcon;
    private Handler mHandler;
    private ARDiscoveryDeviceService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DeviceActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        Button btnMyo = (Button) findViewById(R.id.btn_connect_myo);
        Button btnCalibrate = (Button) findViewById(R.id.btn_calibrate_myo);
        Button btnDrone = (Button) findViewById(R.id.btn_connect_drone);

        myoIcon = (ImageView) findViewById(R.id.image_myo);
        droneIcon = (ImageView) findViewById(R.id.image_drone);

        setMonochrome(myoIcon, 0);
        setMonochrome(droneIcon, 0);


        assert btnMyo != null;
        assert btnCalibrate != null;
        assert btnDrone != null;

        btnMyo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(Const.ACTIVITY_MYO_SCAN);
            }
        });



        btnCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myoHub == null) {
                    Toast.makeText(DeviceActivity.this, getString(R.string.connect_to_myo), Toast.LENGTH_SHORT).show();
                    return;
                }

                startActivity(Const.ACTIVITY_MYO_CALIBRATE);
            }
        });


        btnDrone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myoHub == null) {
                    Toast.makeText(DeviceActivity.this, getString(R.string.connect_to_myo), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (service != null) {
                    Intent intent = new Intent(DeviceActivity.this, DroneActivity.class);
                    intent.putExtra(EXTRA_DEVICE_SERVICE, service);
                    startActivity(intent);
                } else {
                    Toast.makeText(DeviceActivity.this, getString(R.string.connect_to_drone_first), Toast.LENGTH_SHORT).show();
                    setMonochrome(droneIcon, 0);
                }
            }
        });

        mDroneDiscoverer = new DroneDiscover(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        myoHub = App.getMyoHub();

        if (myoHub != null) {

            if (myoHub.getConnectionState() == MyoStates.ConnectionState.CONNECTED) {
                setMonochrome(myoIcon, 1);
            } else {
                setMonochrome(myoIcon, 0);
            }
            myoHub.addConnectionListener(this);
        }

        // setup the drone discoverer and register as listener
        mDroneDiscoverer.setup();
        mDroneDiscoverer.addListener(this);

        // start discovering
        mDroneDiscoverer.startDiscovering();

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (myoHub != null) {
            setMonochrome(myoIcon, 0);
            myoHub.removeConnectionListener(this);
        }

        // clean the drone discoverer object
        mDroneDiscoverer.stopDiscovering();
        mDroneDiscoverer.cleanup();
        mDroneDiscoverer.removeListener(this);
    }

    @Override
    public void onBackPressed() {

        if (myoHub != null && myoHub.getConnectionState() == MyoStates.ConnectionState.CONNECTED) {
            myoHub.shutdown();
        } else {
            super.onBackPressed();
        }
    }

    public void startActivity(int id) {

        Intent intent = null;

        switch (id) {
            case Const.ACTIVITY_MYO_SCAN:
                intent = new Intent(DeviceActivity.this, ScanActivity.class);
                break;
            case Const.ACTIVITY_MYO_CALIBRATE:
                intent = new Intent(DeviceActivity.this, CalibrationActivity.class);

                break;

            case Const.ACTIVITY_DRONE_SCAN:
                intent = new Intent(DeviceActivity.this, DroneScanActivity.class);
                break;
        }


        startActivity(intent);
    }

    @Override
    public void onConnectionStateChanged(MyoGattCallback myoGattCallback, MyoStates.ConnectionState connectionState) {
        Log.i(TAG, connectionState.name());
        switch (connectionState) {
            case DISCONNECTED:
                setMonochrome(myoIcon, 0);
                break;
            case DISCONNECTING:

                break;
            case CONNECTING:

                break;
            case CONNECTED:
                setMonochrome(myoIcon, 1);
                break;
        }
    }

    private void setMonochrome(final ImageView imageView, final int isMono) {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    ColorMatrix matrix = new ColorMatrix();
                    matrix.setSaturation(isMono);

                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                    imageView.setColorFilter(filter);
                }
            });
        }


    }

    @Override
    public void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList) {
        setMonochrome(droneIcon, 0);
        if (!dronesList.isEmpty()) {
            service = dronesList.get(0);
            ARDISCOVERY_PRODUCT_ENUM product = ARDiscoveryService.getProductFromProductID(service.getProductID());

            if (product == ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_ARDRONE || product == ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_BEBOP_2) {
                setMonochrome(droneIcon, 1);
            }
        }
    }
}
