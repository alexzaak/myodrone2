### Available on GooglePLay
[Google Play](https://play.google.com/store/apps/details?id=codes.zaak.myodrone2)

#Gestensteuerung für unbemannte Flugroboter#
*Bachelorarbeit von Alexander Zaak vom 02.08.2016*

###1. Einleitung

Berührungsfrei ein unbemanntes Flugobjekt oder ein Fahrzeug zu steuern, ist kein Science Fiction Szenario mehr. Der Bereich der Mensch-Maschine-Interaktion hat eine beeindruckende Wandlung hinter sich. Von der Lochkarte, über Touchscreen, bis hin zur Gestensteuerung. Dabei stellt die Gestensteuerung eine natürliche und intuitive Eingabemöglichkeit zur Verfügung, sodass grundlegende Befehle für Menschen leicht erlernbar sind. Durch diese besondere Eingabemethode ergeben sich Einsatzgebiete in der Maschinenbau-, Automobil-, Medizin- und in der Unterhaltungsbranche. Um dieses Potential voll und ganz nutzen zu können, bedarf es einer präzisen Erkennung der jeweiligen Geste.

###2. Ausgangslage

Im Rahmen der Veranstaltung „Mobile Softwaresysteme“ wurde im Sommersemester 2015 eine Gestensteuerung in Form einer Android-Anwendung, für Parrots AR.Drone 1.0 und 2.0, entwickelt  ([MyoDrone](http://kasimir.fh-stralsund.de/index.php/180)). Dabei werden vom Myo-Armband Muskelkontraktion erkannt und als Befehle auf die Drohne angewendet. Eine Übersicht der vordefinierten Gesten des Myo-Armbands ist in Abbildung 1 dargestellt. In Kombination mit der inertialen Messeinheit wurden Gesten für die Flugsteuerung fest Vordefiniert und können nicht individuell angepasst werden. Damit das Myo-Armband die gewünschte Aktion präzise erkennt, ist es erforderlich, dass das Armband mittels der Software **Myo Connect**, die von Thalmic Labs Inc. zur Verfügung gestellt wurde, kalibriert wird.


![](img/myo-gestures-new.jpg)
**Quelle**: http://beebom.redkapmedia.netdna-cdn.com/wp-content/uploads/2016/07/myo-gestures.jpg



###3. Zielsetzung

Ziel dieser Arbeit war die Erarbeitung einer Gestensteuerung für Drohnen, die es erlaubt eigene Gesten zu definieren bzw. bestehende zu präzisieren. Durch das Erstellen von personalisierten Gesten soll die Erkennungsrate erhöht und das Sicherheitsgefühl beim Steuern der Drohne gesteigert werden. Darüber hinaus soll der Kalibrierungsvorgang der Gesten auf das Smartphone verlagert werden, damit die Anwendung unabhängig von der Software **Myo Connect** genutzt werden kann.


###4. Umsetzung

Zunächst wurde ein Android-Modul zur Kalibrierung und Gestenerkennung entwickelt, da das standard SDK für das Myo-Armband keine rohen EMG-Daten ausliefert. Die Kommunikation mit dem Myo-Armband wurde mit Hilfe von Myolib gelöst, dass auf [Github](https://github.com/d4rken/myolib) zur Verfügung steht.

Anschließend erfolgte die Implementierung der Drohnensteuerung. Hierfür wurde das von Parrot zur Verfügung gestellte SDK in der Version 3.9.1 benutzt. Des Weiteren wurde für die Entwicklung das aktuelle Android SDK, in der Version 25.1.6, verwendet.

*Die Anwendung umfasst folgende Ansichten:*

Die Startansicht (siehe Abbildung 2) listet die einzelnen Geräte (Myo-Armband, Bebop Drone) auf und signalisiert durch farbig dargestellte Icons, ob eine Verbindung zu dem jeweiligen Gerät besteht (falls ein Gerät nicht verbunden ist, wird das jeweilige Icon grau dargestellt). Des Weiteren kann aus der Startansicht die Drohnen-Ansicht und die Kalibrierungsansicht aufgerufen werden.

Die Kalibrierungsansicht (siehe Abbildung 3) umfasst mehrere Schritte. Zunächst kann eine Kalibrierung der Gesten vorgenommen werden. Hierfür muss die jeweilige Geste ausgeführt und währenddessen der **TAB TO RECORD**-Button bestätigt werden. Sobald eine Geste aufgezeichnet wurde, kann diese auf ihre Genauigkeit geprüft werden.
Danach erfolgt die Kalibrierung der Orientierungsdaten (Roll, Pitch). Hierfür muss der **HOLD TO RECORD**-Button gehalten  und die jeweilige Bewegung ausgeführt werden. Sobald ein optimales Maximum erreicht wurde, kann der Button gelöst werden.
Nachdem de Kalibrierung der Gesten und der Orientierungsdaten erfolgreich war, wird die generierte Steuerung angezeigt.
Anschließend kann die erstellte Steuerung in einem simulierten Szenario auf ihre Genauigkeit getestet werden.

Die Drohnen-Ansicht (siehe Abbildung 4) ist für die Kommunikation und zum Steuern der Drohne zuständig. In dieser Ansicht werden wichtige Informationen, wie der Batteriestatus des Myo-Armbandes und der Drohne, der aktuell ausgeführte Befehl und die aktuell erkannte Geste angezeigt. Des Weiteren kann die Geschwindigkeit der Drohne festgelegt werden und es wurden Buttons zum Notlanden und Starten der Drohne implementiert.

###4. Voraussetzung

* Myo-Armband
* Bebop Drone
* Smartphone mit Android 4.3 oder höher und Bluetooth Low Energy unterstützung

###5. Dokumentation

[JavaDoc](http://alexzaak.bitbucket.io/myodrone2/javadoc/)

###6. Screenshots

![Startansicht der Anwendung](img/screens/startansicht.png =250x)
![Startansicht der Anwendung](img/screens/gesten_kalibrierung.png =250x)
![Kalibrierungsvorgang](img/screens/imu_kalibrierung.png =250x)
![Startansicht der Anwendung](img/screens/steuerbefehle_uebersicht.png =250x)
![Drohenansicht der Anwendung](img/screens/drohne_ansicht.png =250x)

Übersicht aller Funktionen in einem Video:

[YouTube](https://www.youtube.com/embed/VhIn6Es8PRo)



